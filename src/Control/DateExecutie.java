package Control;

import model.*;
import view.*;
import java.util.concurrent.LinkedBlockingQueue;

public class DateExecutie extends Thread {

	private int numarMaxim = 0;
	private int nrCozi;
	private int timpulSimularii;
	private int timpMaximServire;
	private int timpMinimServire;
	private int timpMaximSosire;
	private int timpMinimSosire;

	private LinkedBlockingQueue<Customer> listaClienti = new LinkedBlockingQueue<Customer>();
	public static Queue[] queue;
	private int clienti = 0;
	private static int tmp;
	private static int timpDeAsteptare = 0;
	private int totalServire = 0;
	private int rushHour = 0;
	private int timpTotalFaraClienti = 0;

	public DateExecutie(int nrClienti, int nrCozi, int minArrivingTime, int maxArrivingTime, int minServingTime, int maxServingTime,
			int timpulSimularii) {

		this.clienti = nrClienti;
		this.timpMaximSosire = maxArrivingTime;
		this.timpMinimSosire = minArrivingTime;
		this.timpMaximServire = maxServingTime;
		this.timpMinimServire = minServingTime;
		this.timpulSimularii = timpulSimularii;
		this.nrCozi = nrCozi;

		queue = new Queue[nrCozi];
		int i = 0;
		while (i < nrCozi){
			
			queue[i] = new Queue();
			i++;
		}

	}
	
	public static int obtinereTimp() {
		
		return tmp;
	}
	
	public void adaugareClienti(Customer client) {
		listaClienti.add(client);
	}
	
	public void timpTotalServire(Customer client)
	{
		totalServire += client.getTimpDeServire();
	}

	public static void timpTotalAsteptare(int asteptare) {
		
		timpDeAsteptare += asteptare;
	}

	public static int getTimpTotalAsteptare() {
		
		return timpDeAsteptare;
	}

	public int getFavouriteQueue() {
		
		int minim;
		int index = 0;
		int nr = 0;
		int lungime = queue.length;
		
		if (lungime > 0) {
			
			minim = queue[0].getSize();
			while (index < nrCozi) {
				
				if (queue[index].getSize() < minim) {
					
					minim = queue[index].getSize();
					nr = index;
				}
				index++;
			}
		}
		return nr;
	}
	
	public void setRushHour(int simulationTime) {

		int numar = 0;
		int index = 0;

		while (index < nrCozi) {
			
			numar = numar + queue[index].getSize();
			timpTotalFaraClienti += queue[index].getTimpFaraClienti();
			index++;
		}
		
		if (numar > numarMaxim) {
			
			numarMaxim = numar;
			rushHour = tmp;
		}
	}
	
	public int getNumarMinDeCozi() {
		
		int minim;
		int nrMinim = 0;
		int index = 0;

		while (index < nrCozi) {
		
			if (!queue[index].isOpen()) {
				index++;
				if (index < nrCozi) {
					
					minim = queue[index].getMaxWaitingTime();
					while (index < nrCozi) {
						
						if (queue[index].isOpen()) {
							if (minim > queue[index].getMaxWaitingTime()) {
								
								minim = queue[index].getMaxWaitingTime();
								nrMinim = index;
							}
						}
						index++;
					}
				}
				return nrMinim;
			}
		}
		return -1;
	}

	public void run() {

		int index = 0;
		
		while (index <= clienti) {

			Clienti generator = new Clienti();
			adaugareClienti(generator.genarareClienti(index, timpMaximServire, timpMinimServire, timpMaximSosire, timpMinimSosire));
			index++;
		}
		for (Queue c : queue) {
			
			c.start();
		}	
		tmp = 1;
		while (tmp <= timpulSimularii) {
			
			String t = Integer.toString(tmp);
			GUI.setTimp(t);

			for (Customer client : listaClienti) {
				timpTotalServire( client);
				
				if (client.getTimpDeSosire() == tmp) {
					
					int coadaAleasa = this.getFavouriteQueue();
					GUI.output.append("Clientul " + client.getID() + " se afla la coada " + coadaAleasa + ", a ajuns la momentul de timp "
							+ client.getTimpDeSosire() + ", cu timpul de servire egal cu " + client.getTimpDeServire()
							+ "\n si pleaca la " + client.obtinereTimp() +"!\n");
					queue[coadaAleasa].adaugaClient(client);
					GUI.setTimp2(Integer.toString(tmp / 8));
					}	
				}
			this.setRushHour(timpulSimularii);
			try {
				sleep(1000);
			} catch (InterruptedException exception) {
				
				exception.printStackTrace();
			}
			tmp++;

		}
		GUI.setRush(Integer.toString(rushHour));
		index = 0;
		while (index < nrCozi) {
			
			timpTotalFaraClienti += queue[index].getTimpFaraClienti();
			index++;
		}
		timpTotalFaraClienti = timpTotalFaraClienti / (clienti * nrCozi);
		GUI.setEmpty(Integer.toString(timpTotalFaraClienti));
	}
}
