package view;

import Control.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame {

	private JFrame fereastra = new JFrame("Simulare coada");
	private JLabel label1 = new JLabel("Timp simulare");
	private JLabel label2 = new JLabel("Numar cozi");
	private JLabel label3 = new JLabel("Timpul minim de sosire");
	private JLabel label4 = new JLabel("Timpul maxim de sosire");
	private JLabel label5 = new JLabel("Timpul minim de servire");
	private JLabel label6 = new JLabel("Timpul maxim de servire");
	private JLabel label7 = new JLabel("Timp de executie");

	private JLabel label9 = new JLabel("Ora de varf");
	private JLabel label10 = new JLabel("Timp");
	private JLabel label11 = new JLabel("Numar clienti");

	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();

	private JTextField tmpSimulare = new JTextField("");
	private JTextField numarCozi = new JTextField("");
	private JTextField tmpMaxSosire = new JTextField("");
	private JTextField tmpMinSosire = new JTextField("");
	private JTextField tmpMaxServire = new JTextField("");
	private JTextField tmpMinServire = new JTextField("");
	private JTextField numarClienti = new JTextField("");

	private static JTextField rush = new JTextField("");
	private static JTextField empty = new JTextField("");
	public static JTextField time = new JTextField("");
	public static JTextField time2 = new JTextField("");

	private JButton startBtn = new JButton("Incepe Simularea");
	private JButton exitBtn = new JButton("Exit");

	public static JTextArea output = new JTextArea();

	public GUI() {


		fereastra.getContentPane().setBackground(Color.BLACK);
		fereastra.setBounds(0, 0, 1000, 800);
		fereastra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fereastra.setLayout(new BorderLayout());

		fereastra.add(panel1, BorderLayout.NORTH);
		fereastra.add(panel2,  BorderLayout.CENTER);
		fereastra.add(panel3, BorderLayout.SOUTH);

		panel1.setLayout(new GridLayout(9, 2));
		panel1.setBackground(Color.BLACK);
		panel2.setLayout(null);
		panel3.setLayout(new GridLayout(3,2));

		tmpSimulare.setBounds(150, 50, 100, 30);
		panel1.add(label1);
		panel1.add(tmpSimulare);

		numarCozi.setBounds(200, 100, 100, 30);
		panel1.add(label2);
		panel1.add(numarCozi);

		tmpMinSosire.setBounds(200, 150, 100, 30);
		panel1.add(label3);
		panel1.add(tmpMinSosire);

		tmpMaxSosire.setBounds(200, 200, 100, 30);
		panel1.add(label4);
		panel1.add(tmpMaxSosire);

		tmpMinServire.setBounds(200, 250, 100, 30);
		panel1.add(label5);
		panel1.add(tmpMinServire);

		tmpMaxServire.setBounds(200, 300, 100, 30);
		panel1.add(label6);
		panel1.add(tmpMaxServire);

		time.setBounds(200, 350, 100, 30);
		panel1.add(label7);
		panel1.add(time);

		numarClienti.setBounds(200, 450, 100, 30);
		panel1.add(label11);
		panel1.add(numarClienti);

		startBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				DateExecutie test = new DateExecutie(Integer.parseInt(numarClienti.getText())-1,Integer.parseInt(numarCozi.getText()), Integer.parseInt(tmpMinSosire.getText()),
						Integer.parseInt(tmpMaxSosire.getText()), Integer.parseInt(tmpMinServire.getText()),
						Integer.parseInt(tmpMaxServire.getText()), Integer.parseInt(tmpSimulare.getText()));

				test.start();
				System.out.println(Integer.parseInt(numarCozi.getText()));
			}
		});

		exitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.exit(0);
			}
		});

		output.setBounds(350, 50, 500, 550);
		panel2.add(output);

		panel3.add(label9);
		panel3.add(rush);

		empty.setBounds(150, 600, 100, 30);
		panel3.add(label10);
		panel3.add(empty);
		panel3.add(startBtn);
		panel3.add(exitBtn);

	}

	public static void setTimp(String string) {

		time.setText(string);
	}

	public static void setTimp2(String string) {

		time2.setText(string);
	}

	public static void setRush(String string) {

		rush.setText(string);
	}

	public static void setEmpty(String string) {

		empty.setText(string);
	}

	public static void main(String arg[]) {

		GUI interfataGrafica = new GUI();
		interfataGrafica.fereastra.setVisible(true);
	}
}