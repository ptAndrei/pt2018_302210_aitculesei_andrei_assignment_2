package model;


import java.util.Random;

//Generator
public class Clienti {

	private int nrClienti = 0;
	private int maxServingTime = 0;
	private int minServingTime = 0;
	private int maxArrivingTime = 0;
	private int minArrivingTime = 0;
	private int idClient = 1;

	Random random = new Random();

	public Clienti() {
		// TODO Auto-generated constructor stub
	}
	
	public Clienti(int nrClienti, int maxServingTime, int minServingTime, int maxArrivingTime,
			int minArrivingTime) {
		
		this.nrClienti = nrClienti;
		this.maxServingTime = maxServingTime;
		this.minServingTime = minServingTime;
		this.maxArrivingTime = maxArrivingTime;
		this.minArrivingTime = minArrivingTime;

	}

	public Customer genarareClienti(int idClient, int maxServingTime, int minServingTime, int maxArrivingTime,
			int minArrivingTime) {
		
		int servingTime=0, arrivingTime=0;
		servingTime = random.nextInt((maxServingTime - minServingTime)+1) + minServingTime;
		arrivingTime =  random.nextInt((maxArrivingTime - minArrivingTime)+1) + minArrivingTime;
		System.out.println("Timpul de servire al clientului "+ idClient + " este "+ servingTime+ " iar timpul de sosire este " + arrivingTime);
		Customer client = new Customer(arrivingTime, servingTime, idClient);
		 
		return client;
	}

	public String toStringGen(Customer client)
	{
	
		String afisare = " A fost generat  " + client.toString();
		return afisare;
	}

}
