package model;


public class Customer {
	
	private int timpDeSosire;
	private int ID;
	private int timpDeServire;

	public Customer() {

	}
	
	public Customer(int timpDeSosire, int timpDeServire, int ID) {
		
		this.timpDeSosire = timpDeSosire;
		this.ID = ID;
		this.timpDeServire = timpDeServire;

	}

	public int getID() {
		return ID;
	}

	public int getTimpDeSosire() {
		return timpDeSosire;
	}

	public int getTimpDeServire() {
		return timpDeServire;
	}

	public int obtinereTimp() {
		int tmp;
		tmp = getTimpDeServire() + getTimpDeSosire();

		return tmp;
	}

	public String toString() {
		
		return "Clientul " + getID() + " a ajuns la " + getTimpDeSosire() + " este servit in " + getTimpDeServire() ;
	}


}
