package model;

import java.util.*;
import Control.*;

public class Queue extends Thread {
	private int timpMaximDeAsteptare = 0;
	private int timpFaraClienti = 0;
	private boolean perm;

	private LinkedList<Customer> coada = new LinkedList<Customer>();

	public void setMaxWaitingTime(int maxWaitingTime) {
		
		this.timpMaximDeAsteptare = maxWaitingTime;
	}

	public int getMaxWaitingTime() {
		
		return timpMaximDeAsteptare;
	}

	public int getTimpFaraClienti() {
		return timpFaraClienti;
	}

	public int getSize() {
		return coada.size();
	}

	public void adaugaClient(Customer client) {
		coada.add(client);
		this.setMaxWaitingTime(timpMaximDeAsteptare + client.getTimpDeServire());
	}

	public void stergeClient() {
		
		while (coada.size() == 0) {
			
			coada.removeFirst();
		}
	}

	public void run() {
		while (open(perm)) {
			
			if (coada.isEmpty()) {
				
				try {
					timpFaraClienti++;
					sleep(1000);
				} catch (InterruptedException exception) {
					exception.printStackTrace();
				}
			} else if (!coada.isEmpty()) {
				
				try {
					sleep(coada.getFirst().getTimpDeServire() * 1000);

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				DateExecutie.timpTotalAsteptare(DateExecutie.obtinereTimp() - coada.getFirst().obtinereTimp());
				this.stergeClient();

			}
		}

	}
	
	public boolean open(boolean perm) {
		
		perm = true;	
		return perm;
	}

	public boolean close(boolean perm) {
		
		perm = false;
		return perm;
	}

	public boolean isOpen() {
		
		return perm;
	}
}